# mobfirst-vtex-filters-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirst-vtex-filters-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirst-vtex-filters-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirst-vtex-filters-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-filters-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirst-vtex-filters-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-filters-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirst-vtex-filters-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-filters-pod)

## Example

	Filter *filter1 = [[Filter alloc] initWithStoreUrl:@"http://estoque.com.br" andCategory:@"/infantil/"];
	[filter1 getFacetsFromStoreWithCompletionHandler:^(FacetsReturnObject *facetsReturnObject) {

		Brand *brand = [[facetsReturnObject brands] objectAtIndex:0];
		Brand *brand2 = [[facetsReturnObject brands] objectAtIndex:2];
		[brand setState:YES];
		[brand2 setState:YES];

		SpecificationFilter *filter = [[facetsReturnObject specificationFilters] objectAtIndex:1];
		SpecificationFilterItem *azul = [[filter specificationFilterItems] objectAtIndex:0];
		SpecificationFilterItem *branco = [[filter specificationFilterItems] objectAtIndex:1];

		[azul setState:YES];
		[branco setState:YES];

		NSMutableArray *filterItems = [[NSMutableArray alloc] init];
		[filterItems addObject:azul];
		[filterItems addObject:branco];

		SpecificationFilter *enabledSpecificationFilters = [[SpecificationFilter alloc] initWithName:@"Cor" andItems:filterItems];
		NSArray *specificationFiltersArray = [NSArray arrayWithObject:enabledSpecificationFilters];

		NSMutableArray *brandArray = [NSMutableArray arrayWithObject:brand];
		[brandArray addObject:brand2];

		FacetsReturnObject *enabledFilters = [[FacetsReturnObject alloc] initWithBrands:brandArray specificationFilters:specificationFiltersArray andChildCategories:nil];

		NSString *url = [filter1 getUrlFromFilters:enabledFilters withTextFilter:nil];
		printf("%s", [url UTF8String]);
	}];

## Requirements

## Installation

mobfirst-vtex-filters-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mobfirst-vtex-filters-pod"
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirst-vtex-filters-pod is available under the MIT license. See the LICENSE file for more info.