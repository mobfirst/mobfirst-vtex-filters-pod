//
//  FilterHelper.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 22/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef FilterHelper_h
#define FilterHelper_h

#import "Brand.h"
#import "Category.h"
#import "SpecificationFilterUrlData.h"
#import "SpecificationFilterItem.h"
#import "FacetsReturnObject.h"

@interface FilterHelper : NSObject

+ (NSString *) getBrandUrlFrom:(Brand *) brand;
+ (NSString *) getChildCategoryMapFrom:(Category *) category withBrand:(NSString *) brand;
+ (SpecificationFilterUrlData *) getSpecificationFilterUrlDataFromSpecificationFilter:(SpecificationFilterItem *) filter withCategory:(NSString *) categoryUrl;
+ (NSString *) checkForBrandInFacetsObject:(FacetsReturnObject *) facetsReturnObject;
+ (FacetsReturnObject *) mergeOldFacet:(FacetsReturnObject *) oldObject withNewFacet:(FacetsReturnObject *) newObject;

@end

#endif /* FilterHelper_h */
