//
//  SpecificationFilterUrlData.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 22/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef SpecificationFilterUrlData_h
#define SpecificationFilterUrlData_h

@interface SpecificationFilterUrlData : NSObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *map;

- (id) initWithUrl:(NSString *) url andMap:(NSString *) map;

@end

#endif /* SpecificationFilterUrlData_h */
