//
//  Filter.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Http.h"
#import "Filter.h"
#import "Json.h"
#import "Category.h"

#import "FacetsReturnObject.h"
#import "FilterHelper.h"
#import "SpecificationFilter.h"
#import "SpecificationFilterItem.h"
#import "SpecificationFilterUrlData.h"

@implementation Filter

- (id) initWithStoreUrl:(NSString *)storeUrl andCategory:(NSString *) categoryUrl {
    
    self = [super init];
    
    if(self) {
        
        if([storeUrl containsString:@"?"]) {
            self.storeUrl = [storeUrl componentsSeparatedByString:@"?"][0];
        }
        else {
            self.storeUrl = storeUrl;
        }
        
        if([categoryUrl containsString:@"?"]) {
            self.categoryUrl = [categoryUrl componentsSeparatedByString:@"?"][0];
        }
        else {
            self.categoryUrl = categoryUrl;
        }
        
        self.fetchingCategoryUrl = nil;
        self.brand = nil;
    }
    
    return self;
}

- (void) overrideFetchingCategoryUrl:(NSString *) fetchingCategoryUrl {
    
    if(fetchingCategoryUrl) {
        if([fetchingCategoryUrl containsString:@"?"]) {
            
            self.fetchingCategoryUrl = [fetchingCategoryUrl componentsSeparatedByString:@"?"][0];
        }
        else {
            
            self.fetchingCategoryUrl = fetchingCategoryUrl;
        }
    }
    else {
        self.fetchingCategoryUrl = nil;
    }
}

- (void) getFacetsFromStoreWithCompletionHandler:(void(^)(FacetsReturnObject * facetsReturnObject)) completionHandler {
    
    NSString *categoryUrlToUse = [self fetchingCategoryUrl] ? [self fetchingCategoryUrl] : [self categoryUrl];
    
    if(![self storeUrl] || !categoryUrlToUse) return;
    
    NSString *trimmedStoreUrl = nil;
    NSString *trimmedCategoryUrl = nil;
    
    if([[self storeUrl] hasSuffix:@"/"]) {
        trimmedStoreUrl = [[self storeUrl] substringToIndex:[[self storeUrl] length] - 1];
    }
    else {
        trimmedStoreUrl = [self storeUrl];
    }
    
    if([categoryUrlToUse hasSuffix:@"/"]) {
        trimmedCategoryUrl = [categoryUrlToUse substringToIndex:[categoryUrlToUse length] - 1];
    }
    else {
        trimmedCategoryUrl = categoryUrlToUse;
    }
    
    if([trimmedCategoryUrl hasPrefix:@"/"] && [trimmedCategoryUrl length] > 1) {
        trimmedCategoryUrl = [trimmedCategoryUrl substringFromIndex:1];
    }
    
    if([trimmedStoreUrl length] == 0 || [trimmedCategoryUrl length] == 0) return;
    
    NSMutableString *categoryMap = [NSMutableString stringWithString:@"map="];
    
    NSArray *splittedCategoryUrl = [trimmedCategoryUrl componentsSeparatedByString:@"/"];
    unsigned long categoryLevels = [splittedCategoryUrl count];
    
    for(unsigned long i = 0; i < categoryLevels; i++) {
        
        if([self brand] && [[[splittedCategoryUrl objectAtIndex:i] lowercaseString] isEqualToString:[[self brand] lowercaseString]]) {
            [categoryMap appendString:@"b"];
        }
        else {
            [categoryMap appendString:@"c"];
        }
        
        if(i != categoryLevels - 1) {
            [categoryMap appendString:@","];
        }
    }
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@/api/catalog_system/pub/facets/search/%@?%@", trimmedStoreUrl, trimmedCategoryUrl, categoryMap];
    
    [Http doGetFromUrl:finalUrl withCompletionHandler:^(NSString *response) {
        FacetsReturnObject *returnObject = [Json getFacetsReturnObjectFromJsonString:response];
        
        if(![self brand]) {
            self.brand = [FilterHelper checkForBrandInFacetsObject:returnObject];
            
            if([self brand]) {
                [self getFacetsFromStoreWithCompletionHandler:completionHandler];
            }
            else {
                completionHandler(returnObject);
            }
        }
        else {
            completionHandler(returnObject);
        }
    }];
}

- (NSString *) getUrlFromFilters:(FacetsReturnObject *) filters withTextFilter:(NSString *) text {
    
    if(![self storeUrl] || ![self categoryUrl] || !filters) return nil;
    
    NSString *trimmedStoreUrl = nil;
    NSString *trimmedCategoryUrl = nil;
    
    if([[self storeUrl] hasSuffix:@"/"]) {
        trimmedStoreUrl = [[self storeUrl] substringToIndex:[[self storeUrl] length] - 1];
    }
    else {
        trimmedStoreUrl = [self storeUrl];
    }
    
    if([[self categoryUrl] hasSuffix:@"/"]) {
        trimmedCategoryUrl = [[self categoryUrl] substringToIndex:[[self categoryUrl] length] - 1];
    }
    else {
        trimmedCategoryUrl = [self categoryUrl];
    }
    
    if([trimmedCategoryUrl hasPrefix:@"/"] && [trimmedCategoryUrl length] > 1) {
        trimmedCategoryUrl = [trimmedCategoryUrl substringFromIndex:1];
    }
    
    if([trimmedStoreUrl length] == 0 || [trimmedCategoryUrl length] == 0) return nil;
    
    NSMutableString *finalUrl = nil;
    NSMutableString *map = nil;
    
    // Check for enabled child categories
    if([filters childCategories]) {
        
        for(Category *childCategory in [filters childCategories]) {
            
            if(!childCategory) continue;
            if([childCategory enabled]) {
                
                NSString *trimmedNewCategoryUrl = nil;
                
                if([[childCategory link] hasSuffix:@"/"]) {
                    trimmedNewCategoryUrl = [[childCategory link] substringToIndex:[[childCategory link] length] - 1];
                }
                else {
                    trimmedNewCategoryUrl = [childCategory link];
                }
                
                if([trimmedNewCategoryUrl hasPrefix:@"/"] && [trimmedNewCategoryUrl length] > 1) {
                    trimmedNewCategoryUrl = [trimmedNewCategoryUrl substringFromIndex:1];
                }
                
                if([trimmedNewCategoryUrl length] == 0) continue;
                
                finalUrl = [NSMutableString stringWithFormat:@"%@/api/catalog_system/pub/products/search/%@", trimmedStoreUrl, trimmedNewCategoryUrl];
                NSString *childCategoryMap = [FilterHelper getChildCategoryMapFrom:childCategory withBrand:[self brand]];
                
                if(childCategoryMap) {
                    map = [NSMutableString stringWithString:childCategoryMap];
                }
                else {
                    map = nil;
                }

                break;
            }
        }
    }
    
    if(!finalUrl || !map) {
        
        NSArray *splittedCategoryUrl = [trimmedCategoryUrl componentsSeparatedByString:@"/"];
        unsigned long categoryLevels = [splittedCategoryUrl count];
        
        finalUrl = [NSMutableString stringWithFormat:@"%@/api/catalog_system/pub/products/search/%@", trimmedStoreUrl, trimmedCategoryUrl];
        map = [NSMutableString stringWithString:@"map="];
        
        for(unsigned long i = 0; i < categoryLevels; i++) {
            
            if([self brand] && [[[splittedCategoryUrl objectAtIndex:i] lowercaseString] isEqualToString:[[self brand] lowercaseString]]) {
                [map appendString:@"b"];
            }
            else {
                [map appendString:@"c"];
            }
            
            if(i != categoryLevels - 1) {
                [map appendString:@","];
            }
        }
    }
    
    // Check for enabled brands
    if([filters brands]) {
        
        for(Brand *brand in [filters brands]) {
            if(!brand) continue;
            
            if([brand enabled]) {
                NSString *brandUrl = [FilterHelper getBrandUrlFrom:brand];
                
                [finalUrl appendString:[NSString stringWithFormat:@"/%@", brandUrl]];
                [map appendString:[NSString stringWithFormat:@",b"]];
            }
        }
    }
    
    // Check for enabled filters
    if([filters specificationFilters]) {
        
        for(SpecificationFilter *specificationFilter in [filters specificationFilters]) {
            if(!specificationFilter) continue;
            
            for(SpecificationFilterItem *item in [specificationFilter specificationFilterItems]) {
                if(!item) continue;
                
                if([item enabled]) {
                    SpecificationFilterUrlData *filterData = [FilterHelper getSpecificationFilterUrlDataFromSpecificationFilter:item withCategory:trimmedCategoryUrl];
                    
                    [finalUrl appendString:[NSString stringWithFormat:@"/%@", [filterData url]]];
                    [map appendString:[NSString stringWithFormat:@",%@", [filterData map]]];
                }
            }
        }
    }
    
    if(text) {
        return [NSString stringWithFormat:@"%@?%@&ft=%@", finalUrl, map, text];
    }
    
    return [NSString stringWithFormat:@"%@?%@", finalUrl, map];
}

@end
