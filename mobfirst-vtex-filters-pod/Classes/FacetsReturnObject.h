//
//  FacetsReturnObject.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef FacetsReturnObject_h
#define FacetsReturnObject_h

@interface FacetsReturnObject : NSObject

@property (nonatomic, strong) NSMutableArray *brands;
@property (nonatomic, strong) NSMutableArray *specificationFilters;
@property (nonatomic, strong) NSMutableArray *childCategories;

- (id) initWithBrands:(NSArray *) brands specificationFilters:(NSArray *) specificationFilters andChildCategories:(NSArray *) childCategories;

@end

#endif /* FacetsReturnObject_h */
