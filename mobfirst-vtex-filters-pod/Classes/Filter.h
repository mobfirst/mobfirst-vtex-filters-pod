//
//  Filter.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef Filter_h
#define Filter_h

#import "FacetsReturnObject.h"

@interface Filter : NSObject

@property (nonatomic, strong) NSString *fetchingCategoryUrl;

@property (nonatomic, strong) NSString *storeUrl;
@property (nonatomic, strong) NSString *categoryUrl;

@property (nonatomic, strong) NSString *brand;

- (id) initWithStoreUrl:(NSString *)storeUrl andCategory:(NSString *) categoryUrl;

- (void) overrideFetchingCategoryUrl:(NSString *) fetchingCategoryUrl;

- (void) getFacetsFromStoreWithCompletionHandler:(void(^)(FacetsReturnObject * facetsReturnObject)) completionHandler;
- (NSString *) getUrlFromFilters:(FacetsReturnObject *) filters withTextFilter:(NSString *) text;

@end

#endif /* Filter_h */
