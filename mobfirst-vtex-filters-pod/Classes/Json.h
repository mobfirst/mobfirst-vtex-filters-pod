//
//  Json.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef Json_h
#define Json_h

#import "FacetsReturnObject.h"

@interface Json : NSObject

+ (FacetsReturnObject *) getFacetsReturnObjectFromJsonString:(NSString *) jsonString;

@end

#endif /* Json_h */
