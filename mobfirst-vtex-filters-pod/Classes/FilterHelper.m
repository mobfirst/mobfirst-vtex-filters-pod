//
//  FilterHelper.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 22/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilterHelper.h"
#import "SpecificationFilter.h"

@implementation FilterHelper

+ (NSString *) getBrandUrlFrom:(Brand *) brand {
    
    if(!brand) return nil;
    if(![brand link]) return nil;
    
    NSArray *array = [[brand link] componentsSeparatedByString:@"/"];
    return [array lastObject];
}

+ (NSString *) getChildCategoryMapFrom:(Category *) category withBrand:(NSString *) brand {
    
    NSString *trimmedCategoryUrl = nil;
    if(!category) return nil;
    
    if([[category link] hasSuffix:@"/"]) {
        trimmedCategoryUrl = [[category link] substringToIndex:[[category link] length] - 1];
    }
    else {
        trimmedCategoryUrl = [category link];
    }
    
    if([trimmedCategoryUrl hasPrefix:@"/"] && [trimmedCategoryUrl length] > 1) {
        trimmedCategoryUrl = [trimmedCategoryUrl substringFromIndex:1];
    }
    
    if([trimmedCategoryUrl length] == 0) return nil;
    NSMutableString *map = [NSMutableString stringWithString:@"map="];
    
    NSArray *splittedCategoryUrl = [trimmedCategoryUrl componentsSeparatedByString:@"/"];
    unsigned long categoryLevels = [splittedCategoryUrl count];
    
    for(unsigned long i = 0; i < categoryLevels; i++) {
        
        if(brand && [[[splittedCategoryUrl objectAtIndex:i] lowercaseString] isEqualToString:[brand lowercaseString]]) {
            [map appendString:@"b"];
        }
        else {
            [map appendString:@"c"];
        }
        
        if(i != categoryLevels - 1) {
            [map appendString:@","];
        }
    }
    
    return map;
}

+ (SpecificationFilterUrlData *) getSpecificationFilterUrlDataFromSpecificationFilter:(SpecificationFilterItem *) filter withCategory:(NSString *) categoryUrl {
    
    if(!categoryUrl || !filter) return nil;
    if(![filter link]) return nil;
    
    NSString *link = [filter link];
    
    NSString *linkWithoutCategory = [link stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"/%@/", categoryUrl] withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [link length])];
    
    NSArray *questionMarkSplit = [linkWithoutCategory componentsSeparatedByString:@"?"];
    NSArray *commaSplit = [linkWithoutCategory componentsSeparatedByString:@","];
    
    return [[SpecificationFilterUrlData alloc] initWithUrl:[questionMarkSplit firstObject] andMap:[commaSplit lastObject]];
}

+ (NSString *) checkForBrandInFacetsObject:(FacetsReturnObject *) facetsReturnObject {
    
    if(!facetsReturnObject) return nil;
    if(![facetsReturnObject brands]) return nil;
    
    for(int i = 0; i < [[facetsReturnObject brands] count]; i++) {
        
        Brand *brand = [[facetsReturnObject brands] objectAtIndex:i];
        
        NSString *link = [brand link];
        NSString *trimmedLink = nil;
        
        if([link hasSuffix:@"/"]) {
            trimmedLink = [link substringToIndex:[link length] - 1];
        }
        else {
            trimmedLink = link;
        }
        
        if([trimmedLink hasPrefix:@"/"] && [trimmedLink length] > 1) {
            trimmedLink = [trimmedLink substringFromIndex:1];
        }
        
        NSArray *splittedLink = [trimmedLink componentsSeparatedByString:@"/"];
        NSMutableSet *set = [[NSMutableSet alloc] init];
        
        for(NSString *linkComponent in splittedLink) {
            
            if([set containsObject:[linkComponent lowercaseString]]) {
                return linkComponent;
            }
            else {
                [set addObject:[linkComponent lowercaseString]];
            }
        }
    }
    
    return nil;
}

+ (FacetsReturnObject *) mergeOldFacet:(FacetsReturnObject *) oldObject withNewFacet:(FacetsReturnObject *) newObject {
    
    if(!newObject) return nil;
    if(!oldObject) return newObject;
    
    if([newObject childCategories] && [oldObject childCategories]) {
        for(int i = 0; i < [[newObject childCategories] count]; i++) {
            
            for(int j = 0; j < [[oldObject childCategories] count]; j++) {
                if([[[[[newObject childCategories] objectAtIndex:i] name] lowercaseString] isEqualToString:[[[[oldObject childCategories] objectAtIndex:j] name] lowercaseString]]) {
                    
                    [[newObject childCategories] replaceObjectAtIndex:i withObject:[[oldObject childCategories] objectAtIndex:j]];
                }
            }
        }
    }
    
    if([newObject brands] && [oldObject brands]) {
        
        for(int i = 0; i < [[newObject brands] count]; i++) {
            
            for(int j = 0; j < [[oldObject brands] count]; j++) {
                if([[[[[newObject brands] objectAtIndex:i] name] lowercaseString] isEqualToString:[[[[oldObject brands] objectAtIndex:j] name] lowercaseString]]) {
                    
                    [[newObject brands] replaceObjectAtIndex:i withObject:[[oldObject brands] objectAtIndex:j]];
                }
            }
        }
    }
    
    if([newObject specificationFilters] && [oldObject specificationFilters]) {
        for(int i = 0; i < [[newObject specificationFilters] count]; i++) {
            
            for(int j = 0; j < [[oldObject specificationFilters] count]; j++) {
                if([[[[[newObject specificationFilters] objectAtIndex:i] name] lowercaseString] isEqualToString:[[[[oldObject specificationFilters] objectAtIndex:j] name] lowercaseString]]) {
                    
                    for(int x = 0; x < [[[[newObject specificationFilters] objectAtIndex:i] specificationFilterItems] count]; x++) {
                        for(int y = 0; y < [[[[oldObject specificationFilters] objectAtIndex:j] specificationFilterItems] count]; y++) {
                            
                            if([[[[[[[newObject specificationFilters]objectAtIndex:i] specificationFilterItems] objectAtIndex:x] name] lowercaseString] isEqualToString:[[[[[[oldObject specificationFilters] objectAtIndex:j] specificationFilterItems] objectAtIndex:y] name]lowercaseString]]) {
                                
                                [[[[newObject specificationFilters] objectAtIndex:i] specificationFilterItems] replaceObjectAtIndex:x withObject:[[[[oldObject specificationFilters] objectAtIndex:j] specificationFilterItems] objectAtIndex:y]];
                            }
                        }
                    }
                }
            }
        }
    }
    
    return newObject;
}

@end
