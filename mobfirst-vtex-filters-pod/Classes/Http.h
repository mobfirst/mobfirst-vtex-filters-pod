//
//  Http.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

@interface Http : NSObject

+ (void) doGetFromUrl:(NSString *) url withCompletionHandler:(void(^)(NSString * response)) completionHandler;

@end
