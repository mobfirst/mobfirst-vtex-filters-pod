//
//  SpecificationFilter.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef SpecificationFilter_h
#define SpecificationFilter_h

#include "SpecificationFilterItem.h"

@interface SpecificationFilter : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *specificationFilterItems;

- (id) initWithName:(NSString *) name andItems:(NSArray *) items;

@end

#endif /* SpecificationFilter_h */
