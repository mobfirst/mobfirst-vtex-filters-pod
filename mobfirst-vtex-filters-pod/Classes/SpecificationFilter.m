//
//  SpecificationFilter.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpecificationFilter.h"

@implementation SpecificationFilter

- (id) initWithName:(NSString *) name andItems:(NSMutableArray *) items {
    
    self = [super init];
    
    if(self) {
        
        self.name = name;
        self.specificationFilterItems = items;
    }
    
    return self;
}

@end
