//
//  Json.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Json.h"
#import "FacetsReturnObject.h"
#import "Brand.h"
#import "SpecificationFilter.h"
#import "Category.h"

@implementation Json

+ (FacetsReturnObject *) getFacetsReturnObjectFromJsonString:(NSString *) jsonString {
    
    if(!jsonString) return nil;
    
    NSError *error;
    
    NSMutableArray *brands = [[NSMutableArray alloc] init];
    NSMutableArray *specificationFilters = [[NSMutableArray alloc] init];
    NSMutableArray *childCategories = [[NSMutableArray alloc] init];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if(!jsonDictionary) return nil;
    if(error) return nil;
    
    // Get brands
    if([jsonDictionary objectForKey:@"Brands"]) {
        
        NSArray *brandsArray = [jsonDictionary objectForKey:@"Brands"];
        if(brandsArray) {
            
            for(NSDictionary *brandItemDictionary in brandsArray) {
                if(!brandItemDictionary) continue;
                
                NSString *name = [brandItemDictionary objectForKey:@"Name"];
                NSString *link = [brandItemDictionary objectForKey:@"Link"];
                
                long quantity = [[brandItemDictionary objectForKey:@"Quantity"] longValue];
                if(!name || !link) continue;
                
                Brand *brandItem = [[Brand alloc] initWithName:name link:link andQuantity:quantity];
                [brands addObject:brandItem];
            }
        }
    }
    
    // Get specification filters
    if([jsonDictionary objectForKey:@"SpecificationFilters"]) {
        
        // Get available filter kinds
        NSDictionary *specificationFiltersDictionary = [jsonDictionary objectForKey:@"SpecificationFilters"];
        if(specificationFiltersDictionary) {
         
            for(id key in specificationFiltersDictionary) {
                NSMutableArray *specificationFilterItems = [[NSMutableArray alloc] init];
                
                NSArray *specificationFilterKindArray = [specificationFiltersDictionary objectForKey:key];
                if(!specificationFilterKindArray) continue;
                
                for(NSDictionary *specificationFilterKindDictionary in specificationFilterKindArray) {
                    if(!specificationFilterKindDictionary) continue;
                    
                    NSString *name = [specificationFilterKindDictionary objectForKey:@"Name"];
                    NSString *link = [specificationFilterKindDictionary objectForKey:@"Link"];
                    
                    long quantity = [[specificationFilterKindDictionary objectForKey:@"Quantity"] longValue];
                    if(!name || !link) continue;
                    
                    SpecificationFilterItem *specificationFilterItem = [[SpecificationFilterItem alloc] initWithName:name link:link andQuantity:quantity];
                    [specificationFilterItems addObject:specificationFilterItem];
                }
                
                SpecificationFilter *specificationFilter = [[SpecificationFilter alloc] initWithName:key andItems:specificationFilterItems];
                [specificationFilters addObject:specificationFilter];
            }
        }
    }
    
    // Get child categories
    if([jsonDictionary objectForKey:@"CategoriesTrees"]) {
        
        NSArray *categoriesTreesArray = [jsonDictionary objectForKey:@"CategoriesTrees"];
        if(categoriesTreesArray) {
            
            if([categoriesTreesArray count] > 0) {
                NSDictionary *currentCategory = [categoriesTreesArray objectAtIndex:0];
                
                if(currentCategory) {
                    if([currentCategory objectForKey:@"Children"]) {
                        
                        NSArray *childCategoriesArray = [currentCategory objectForKey:@"Children"];
                        if(childCategoriesArray) {
                            
                            for(NSDictionary *childCategoryItemDictionary in childCategoriesArray) {
                                if(!childCategoryItemDictionary) continue;
                                
                                NSString *name = [childCategoryItemDictionary objectForKey:@"Name"];
                                NSString *link = [childCategoryItemDictionary objectForKey:@"Link"];
                                
                                long quantity = [[childCategoryItemDictionary objectForKey:@"Quantity"] longValue];
                                if(!name || !link) continue;
                                
                                Category *childCategoryItem = [[Category alloc] initWithName:name link:link andQuantity:quantity];
                                [childCategories addObject:childCategoryItem];
                            }
                        }
                    }
                }
            }
        }
    }
    
    FacetsReturnObject *returnObject = [[FacetsReturnObject alloc] initWithBrands:brands specificationFilters:specificationFilters andChildCategories:childCategories];
    return returnObject;
}

@end
