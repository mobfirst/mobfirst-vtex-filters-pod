//
//  SpecificationFilterUrlData.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 22/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpecificationFilterUrlData.h"

@implementation SpecificationFilterUrlData

- (id) initWithUrl:(NSString *) url andMap:(NSString *) map {
    
    self = [super init];
    
    if(self) {
        
        self.url = url;
        self.map = map;
    }
    
    return self;
}

@end
