//
//  FacetsReturnObject.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacetsReturnObject.h"

@implementation FacetsReturnObject

- (id) initWithBrands:(NSArray *) brands specificationFilters:(NSArray *) specificationFilters andChildCategories:(NSArray *) childCategories {
    
    self = [super init];
    
    if(self) {
        
        self.brands = [[NSMutableArray alloc] initWithArray:brands];
        self.specificationFilters = [[NSMutableArray alloc] initWithArray:specificationFilters];
        self.childCategories = [[NSMutableArray alloc] initWithArray:childCategories];
    }
    
    return self;
}

@end
