//
//  SpecificationFilterItem.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 22/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef SpecificationFilterItem_h
#define SpecificationFilterItem_h

@interface SpecificationFilterItem : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *link;
@property (nonatomic) long quantity;
@property (nonatomic) BOOL enabled;

- (id) initWithName:(NSString *) name link:(NSString *) link andQuantity:(long) quantity;
- (void) setState:(BOOL) enabled;

@end

#endif /* SpecificationFilterItem_h */
