//
//  Category.h
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef Category_h
#define Category_h

@interface Category : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *link;
@property (nonatomic) long quantity;
@property (nonatomic) BOOL enabled;

- (id) initWithName:(NSString *) name link:(NSString *) link andQuantity:(long) quantity;
- (void) setState:(BOOL) enabled;

@end

#endif /* Category_h */
