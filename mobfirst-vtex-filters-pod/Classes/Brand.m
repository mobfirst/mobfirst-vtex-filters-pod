//
//  Brand.m
//  VtexFiltersTest
//
//  Created by Teste Eclipse 2 on 21/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Brand.h"

@implementation Brand

- (id) initWithName:(NSString *) name link:(NSString *) link andQuantity:(long) quantity {
    
    self = [super init];
    
    if(self) {
        
        self.name = name;
        self.link = link;
        self.quantity = quantity;
        self.enabled = NO;
    }
    
    return self;
}

- (void) setState:(BOOL) enabled {
    self.enabled = enabled;
}

@end
